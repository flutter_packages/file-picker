package com.mr.flutter.plugin.logs;

//import io.bekey.aadvark.app.BuildConfig;

/**
 * Created by Anton Petrusha on 24.01.2018
 * LogCat with active link to java files by string position
 * Example:
 * i(this, "### onCreate ###");
 *  01-24 16:37:34.059 31142-31142/io.bekey.cryptodemo I/(MainActivity.java:81) java-ndk.MainActivity.onCreate[81]: ### onCreate ###
 */


public class Log {

    private static final String TAG = "Android:";
    private static boolean logEnabled = false; //(BuildConfig.enableConsoleLog)?true:false;;

    public static void i(Object thisClass, String string){

        if (logEnabled) android.util.Log.i(getTAG(thisClass), string);
    }

    public static void e(Object thisClass, String string){
        if (logEnabled) android.util.Log.e(getTAG(thisClass), string);
    }

    public static void d(Object thisClass, String string){
        if (logEnabled) android.util.Log.d(getTAG(thisClass), string);
    }

    public static void v(Object thisClass, String string){
        if (logEnabled) android.util.Log.v(getTAG(thisClass), string);
    }

    public static void w(Object thisClass, String string){
        if (logEnabled) android.util.Log.w(getTAG(thisClass), string);
    }

    public static void i(String string) {
        if (logEnabled) android.util.Log.i(TAG, string);
    }

    public static void e(String string) {
        if (logEnabled) android.util.Log.e(TAG, string);
    }

    public static void e(String string, Exception e) {
        if (logEnabled) android.util.Log.e(TAG, string, e);
    }

    public static void d(String string) {
        if (logEnabled) android.util.Log.d(TAG, string);
    }

    public static void d(String TAG, String string) {
        if (logEnabled) {
           System.out.println(TAG + string);
        }
    }

    public static void v(String string) {
        if (logEnabled) android.util.Log.v(TAG, string);
    }

    public static void w(String string) {
        if (logEnabled) android.util.Log.w(TAG, string);
    }

    public static String getTAG(Object o) {
        StackTraceElement[] elements = Thread.currentThread().getStackTrace();
        if (o.getClass().equals(String.class)){
            return TAG + "." + o + "";
        }
        String cName = o.getClass().getSimpleName();
        cName = (cName.length() > 0)?cName:o.getClass().getName();
        int position = 0;
        for (int i = 0; i < elements.length; i++) {
            if ((elements[i].getFileName().contains(cName) && !elements[i+1].getFileName().contains(cName)) || elements[i].getClassName().equals(cName)){
                position = i;
                break;
            }
        }

        StackTraceElement element = elements[position];
        String className = element.getFileName().replace(".java", "");
        return "("+element.getFileName()+":"+element.getLineNumber()+") " + TAG + "." + className + "." + element.getMethodName() + "[" + element.getLineNumber() + "]";
    }
}


